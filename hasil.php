<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Input nilai matakuliah</title>
  </head>
  <body>

  <div class="position-absolute top-50 start-50 translate-middle">
    <h1>Berikut adalah nilai kamu</h1>

    <?php
        $nama = $_POST['nama'];
        $matkul = $_POST['matkul'];
        $uts = $_POST['uts'];
        $uas = $_POST['uas'];
        $tugas = $_POST['tugas'];

        $utstotal = $uts * 0.35;
        $uastotal = $uas * 0.50;
        $tugastotal= $tugas * 0.15;

        $nilai_total = $utstotal + $uastotal + $tugastotal;

        if($nilai_total >=90) {
          $grade = "A";
        }

        elseif($nilai_total >=70) {
          $grade = "B";
        }

        elseif($nilai_total >=50) {
          $grade = "C";
        }
        elseif($nilai_total >=0) {
          $grade = "D";
        }

    ?>  

    <h4><?php echo "Nama : $nama <br>"; 
    echo "Mata Pelajaran : $matkul <br>";?></h4>
    
    <div>
<table class="table">
  <tbody>
    <tr>
      <th scope="row">UTS</th> 
      <td colspan="2" class="table-active"><?php echo $uts ?> </td> 
    </tr>
    
    <tr>
      <th scope="row">UAS</th>
      <td colspan="2" class="table-active"><?php echo $uas ?> </td>
    </tr>

    <tr>
      <th scope="row">Tugas</th>
      <td colspan="2" class="table-active"><?php echo $tugas ?> </td>
    </tr>

    <tr>
      <th scope="row">Total</th>
      <td colspan="2" class="table-active"><?php echo $nilai_total ?> </td>
    </tr>

    <tr>
      <th scope="row">Grade</th>
      <td colspan="2" class="table-dark"><?php echo $grade ?> </td>
    </tr>

  </tbody>
</table>

<br>
    </div>
      <form form action="input.php" method="post">
        <div class="d-grid gap-2">
       <button class="btn btn-primary" type="submit">kembali</button>
       </div>
      </form>
    </div>
 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
   
  </body>
</html>